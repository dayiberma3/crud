package lourdes.macias.bermudez.pm.facci.crud

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity(), View.OnClickListener {

    private var ruta: EditText? = null
    private var origen: EditText? = null
    private var destino: EditText? = null
    private var compania: EditText? = null
    private var tiempo: EditText? = null
    private var id: EditText? = null
    private var guardar: Button? = null
    private var consultar: Button? = null
    private var modificar: Button? = null
    private var eliminarT: Button? = null
    private var Datos: TextView? = null
    private var dataBase: DataBase? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        ruta = findViewById(R.id.ruta) as EditText
        origen = findViewById(R.id.origen) as EditText
        destino = findViewById(R.id.destino) as EditText
        compania = findViewById(R.id.compania) as EditText
        tiempo = findViewById(R.id.tiempo) as EditText
        id = findViewById(R.id.Id) as EditText
        guardar = findViewById(R.id.Guardar) as Button
        consultar = findViewById(R.id.consultar) as Button
        modificar = findViewById(R.id.modificar) as Button
        eliminarT = findViewById(R.id.EliminarT) as Button

        guardar!!.setOnClickListener(this)
        consultar!!.setOnClickListener(this)
        modificar!!.setOnClickListener(this)
        eliminarT!!.setOnClickListener(this)
        Datos = findViewById(R.id.Datos) as TextView

        dataBase = DataBase(this)

    }

    override fun onClick(v: View) {

        when (v.id) {
            R.id.Guardar ->

                if (ruta!!.text.toString().isEmpty()) {

                } else if (ruta!!.text.toString().isEmpty()) {

                } else if (origen!!.text.toString().isEmpty()) {

                } else if (destino!!.text.toString().isEmpty()) {

                } else if (compania!!.text.toString().isEmpty()) {

                } else if (tiempo!!.text.toString().isEmpty()) {

                } else {
                    dataBase!!.Insertar(
                        ruta!!.text.toString(), origen!!.text.toString(),
                        destino!!.text.toString(), compania!!.text.toString(), tiempo!!.text.toString()
                    )
                    Toast.makeText(this, "GUARDADO", Toast.LENGTH_SHORT).show()
                    id!!.setText("")
                    ruta!!.setText("")
                    origen!!.setText("")
                    destino!!.setText("")
                    compania!!.setText("")
                    tiempo!!.setText("")
                    Datos!!.text = ""
                }

            R.id.consultar -> {
                Datos!!.setText(dataBase!!.consultarTodos())
                id!!.setText("")
                ruta!!.setText("")
                origen!!.setText("")
                destino!!.setText("")
                compania!!.setText("")
                tiempo!!.setText("")
                Toast.makeText(this, "LISTADOS", Toast.LENGTH_SHORT).show()
            }


            R.id.modificar -> if (ruta!!.text.toString().isEmpty()) {

            } else if (ruta!!.text.toString().isEmpty()) {

            } else if (origen!!.text.toString().isEmpty()) {

            } else if (destino!!.text.toString().isEmpty()) {

            } else if (compania!!.text.toString().isEmpty()) {

            } else if (tiempo!!.text.toString().isEmpty()) {

            } else if (id!!.text.toString().isEmpty()) {

            } else {
                dataBase!!.Modificar(
                    id!!.text.toString(), ruta!!.text.toString(), origen!!.text.toString(),
                    destino!!.text.toString(), compania!!.text.toString(), tiempo!!.text.toString()
                )
                Toast.makeText(this, "ACTUALIZADO", Toast.LENGTH_SHORT).show()
                id!!.setText("")
                ruta!!.setText("")
                origen!!.setText("")
                destino!!.setText("")
                compania!!.setText("")
                tiempo!!.setText("")
                Datos!!.text = ""
            }

            R.id.EliminarT -> {
                dataBase!!.EliminarTodo()
                Datos!!.text = ""
                Toast.makeText(this, "ELIMINADOS", Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onPointerCaptureChanged(hasCapture: Boolean) {

    }
}

