package lourdes.macias.bermudez.pm.facci.crud;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;

public class DataBase extends SQLiteOpenHelper {

    private static int version = 1;
    private static String name = "Prueba" ;
    private static SQLiteDatabase.CursorFactory factory = null;

    public DataBase(Context context) {
        super(context, name, factory, version);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE RUTAS(" +
                "id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "ruta VARCHAR," +
                "origen VARCHAR," +
                "destino VARCHAR," +
                "compania VARCHAR, " +
                "tiempo VARCHAR)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String sql = "DROP TABLE IF EXISTS RUTAS";
        db.execSQL(sql);
        onCreate(db);
    }
    public void Insertar(String ruta, String origen, String destino, String compania, String tiempo){

        SQLiteDatabase database = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put("ruta", ruta);
        contentValues.put("origen", origen);
        contentValues.put("destino", destino);
        contentValues.put("compania", compania);
        contentValues.put("tiempo", tiempo);
        database.insert("RUTAS",null,contentValues);
    }
    public void EliminarTodo(){
        SQLiteDatabase database = this.getWritableDatabase();
        database.delete("RUTAS", null, null);
    }
    public void Modificar(String id, String ruta, String origen, String destino, String compania, String tiempo){
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("ruta", ruta);
        contentValues.put("origen", origen);
        contentValues.put("destino", destino);
        contentValues.put("compania", compania);
        contentValues.put("tiempo", tiempo);
        database.update("RUTAS", contentValues, "id= " + id, null);
    }
    public String consultarTodos() {
        SQLiteDatabase database = this.getReadableDatabase();
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        String result = "";
        String[] sqlSelect = {"id","ruta", "origen", "destino", "compania", "tiempo"};
        String sqlTable = "RUTAS";
        qb.setTables(sqlTable);
        Cursor c = qb.query(database, sqlSelect, null, null, null, null, null);
        if (c.moveToFirst()) {
            do {
                result += "id " + c.getInt(c.getColumnIndex("id")) + "\n" +
                        "ruta " + c.getString(c.getColumnIndex("ruta")) + "\n" +
                        "origen " + c.getString(c.getColumnIndex("origen")) + "\n" +
                        "destino " + c.getString(c.getColumnIndex("destino")) + "\n" +
                        "compania " + c.getString(c.getColumnIndex("compania")) + "\n" +
                        "tiempo " + c.getString(c.getColumnIndex("tiempo")) + "\n\n\n";
            } while (c.moveToNext());
            c.close();
            database.close();
        }
        return result;

    }
}
